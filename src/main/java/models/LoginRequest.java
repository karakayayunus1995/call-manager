package models;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class LoginRequest implements Serializable {

    @Getter
    @Setter
    @SerializedName("phone")
    private String phone;

    @Getter
    @Setter
    @SerializedName("password")
    private String password;

    public LoginRequest(String phone, String password) {
        this.phone = phone;
        this.password = password;
    }
}
