package models;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class User implements Serializable {

    @Getter
    @Setter
    @SerializedName("id")
    private int id;

    @Getter
    @Setter
    @SerializedName("name")
    private String name;

    @Getter
    @Setter
    @SerializedName("phone")
    private String phone;

    @Getter
    @Setter
    @SerializedName("status")
    private int status;

    @Getter
    @Setter
    @SerializedName("gps_id")
    private String gpsId;

    @Getter
    @Setter
    @SerializedName("start_date")
    private String startDate;

    @Getter
    @Setter
    @SerializedName("end_date")
    private String endDate;

    @Getter
    @Setter
    @SerializedName("created_at")
    private String createdAt;

    @Getter
    @Setter
    @SerializedName("updated_at")
    private String updatedAt;

}
