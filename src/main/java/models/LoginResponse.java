package models;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    @Getter
    @Setter
    @SerializedName("access_token")
    private String accessToken;

    @Getter
    @Setter
    @SerializedName("token_type")
    private String tokenType;

    @Getter
    @Setter
    @SerializedName("expires_token_in")
    private int expiresTokenIn;

    @Getter
    @Setter
    @SerializedName("user")
    private User user;

}
