package customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Customer")
@Data
@AllArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @Column(name = "name", nullable = false)
    @Getter
    @Setter
    private String name;

    @Column(name = "telephone", nullable = false)
    @Getter
    @Setter
    private String telephone;

    @Column(name = "address")
    @Getter
    @Setter
    private String address;

    @Column(name = "note")
    @Getter
    @Setter
    private String note;

    @Column(name = "creation_date", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Getter
    @Setter
    private Date creationDate;

    public Customer() {
    }

    public Customer(String name, String telephone, String address, String note) {
        this.name = name;
        this.telephone = telephone;
        this.address = address;
        this.note = note;
    }

    public void setCustomer(Customer customer) {
        this.name = customer.name;
        this.telephone = customer.telephone;
        this.address = customer.address;
        this.note = customer.note;
    }
}
