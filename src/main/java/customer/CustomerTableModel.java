package customer;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class CustomerTableModel extends AbstractTableModel {

    private final String[] columnNames = {
            "Müşteri", "Telefon", "Adres", "Not"
    };

    private final List<Customer> customerList;

    public CustomerTableModel(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public int getRowCount() {
        return customerList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return customerList.get(rowIndex).getName();
            case 1:
                return customerList.get(rowIndex).getTelephone();
            case 2:
                if (customerList.get(rowIndex).getAddress() != null)
                    return customerList.get(rowIndex).getAddress();
                else
                    return "";
            case 3:
                if (customerList.get(rowIndex).getNote() != null)
                    return customerList.get(rowIndex).getNote();
                else
                    return "";
            default:
                return customerList.get(rowIndex).getId();
        }
    }

}
