package customer;

import main.EventInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;

public class CustomerRightClick extends JPopupMenu {

    public CustomerRightClick(Customer customer, EventInterface eventInterface) {

        ActionListener copyAction = e -> {
            StringSelection stringSelection = new StringSelection(customer.getName() + "\n" + customer.getTelephone());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        };

        ActionListener copyWithAddressAction = e -> {
            StringSelection stringSelection = new StringSelection(customer.getName() + "\n" + customer.getTelephone() + "\n" + customer.getAddress());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        };

        ActionListener copyWithNoteAction = e -> {
            StringSelection stringSelection;
            if (customer.getAddress() != null) {
                stringSelection = new StringSelection(customer.getName() + "\n" + customer.getTelephone() + "\n" + customer.getAddress() + "\n" + customer.getNote());
            } else {
                stringSelection = new StringSelection(customer.getName() + "\n" + customer.getTelephone() + "\n" + customer.getNote());
            }
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        };

        ActionListener updateAction = e -> eventInterface.updateCustomer(customer);

        ActionListener deleteAction = e -> {
            String message = "Müşteriyi silmek istediğinize emin misiniz?";
            int reply = JOptionPane.showConfirmDialog(null, message, null, JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {
                eventInterface.deleteCustomer(customer);
            }
        };

        JMenu copyMenu = new JMenu("Kopyala");

        JMenuItem copy = copyMenu.add(new JMenuItem("Telefon ile"));
        copy.addActionListener(copyAction);

        if (customer.getAddress() != null) {
            JMenuItem copyWithAddress = copyMenu.add(new JMenuItem("Telefon ve adres ile"));
            copyWithAddress.addActionListener(copyWithAddressAction);
        }

        if (customer.getNote() != null) {
            JMenuItem copyWithAddress;
            if (customer.getAddress() != null) {
                copyWithAddress = copyMenu.add(new JMenuItem("Telefon, adres ve not ile"));
            } else {
                copyWithAddress = copyMenu.add(new JMenuItem("Telefon ve not ile"));
            }
            copyWithAddress.addActionListener(copyWithNoteAction);
        }

        add(copyMenu);

        JMenuItem update = add(new JMenuItem("Güncelle"));
        update.addActionListener(updateAction);

        JMenuItem delete = add(new JMenuItem("Sil"));
        delete.addActionListener(deleteAction);

    }

}