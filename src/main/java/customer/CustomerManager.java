package customer;

import main.CallManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;

public class CustomerManager {

    public void onlyAddCustomer(Customer customer) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Customer> addCustomer(Customer customer) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<Customer> customers = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            session.save(customer);
            customers = session.createQuery("FROM Customer").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customers;
    }

    public List<Customer> listCustomers() {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<Customer> customers = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            customers = session.createQuery("FROM Customer").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customers;
    }

    public List<Customer> searchCustomers(String name, String telephone, String address, String note) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<Customer> customers = Collections.emptyList();

        try {
            transaction = session.beginTransaction();   // and telephone = :telephone and address = :address and note = :note
            String nameSearch = StringUtils.isNotBlank(name) ? " name LIKE '%" + name + "%'" : "";
            String telephoneSearch = StringUtils.isNotBlank(telephone) ? " and telephone LIKE '%" + telephone + "%'" : "";
            String addressSearch = StringUtils.isNotBlank(address) ? " and address LIKE '%" + address + "%'" : "";
            String noteSearch = StringUtils.isNotBlank(note) ? " and note LIKE '%" + note + "%'" : "";
            String condition = "FROM Customer WHERE" + nameSearch + telephoneSearch + addressSearch + noteSearch;
            System.out.println(condition);
            Query query = session.createQuery(condition);
            customers = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customers;
    }

    public List<Customer> updateCustomer(Customer customer) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<Customer> customers = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            Customer newCustomer = session.get(Customer.class, customer.getId());
            newCustomer.setCustomer(customer);
            session.update(newCustomer);
            customers = session.createQuery("FROM Customer").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customers;
    }

    public List<Customer> deleteCustomer(Integer customerID) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<Customer> customers = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            Customer customer = session.get(Customer.class, customerID);
            session.delete(customer);
            customers = session.createQuery("FROM Customer ORDER BY name DESC").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customers;
    }

    // TODO: 18.08.2021 İsme ve tarihe göre sıralama ekle

}