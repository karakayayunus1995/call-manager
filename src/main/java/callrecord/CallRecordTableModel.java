package callrecord;

import customer.Customer;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.List;

public class CallRecordTableModel extends AbstractTableModel {

    private final String[] columnNames = {
            "Tarih", "Müşteri", "Adres", "Not", "Takma Adı", "Durum Bilgisi"
    };

    private final List<CallRecord> callRecordList;

    public CallRecordTableModel(List<CallRecord> callRecordList) {
        this.callRecordList = callRecordList;
    }

    @Override
    public int getRowCount() {
        return callRecordList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Customer customer = null;
        if (callRecordList.get(rowIndex).getCustomerId() != null) {
            customer = callRecordList.get(rowIndex).getCustomerId();
        }
        switch (columnIndex) {
            case 0:
                if (callRecordList.get(rowIndex).getCreationDate() != null) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    return simpleDateFormat.format(callRecordList.get(rowIndex).getCreationDate());
                } else
                    return "";
            case 1:
                if (customer != null)
                    return customer.getName();
                else
                    return callRecordList.get(rowIndex).getTelephone();
            case 2:
                if (customer != null)
                    return customer.getAddress();
                else
                    return "";
            case 3:
                if (callRecordList.get(rowIndex).getNote() != null)
                    return callRecordList.get(rowIndex).getNote();
                else
                    return "";
            case 4:
                if (callRecordList.get(rowIndex).getNickName() != null)
                    return callRecordList.get(rowIndex).getNickName();
                else
                    return "";
            case 5:
                if (callRecordList.get(rowIndex).getState() != null)
                    return callRecordList.get(rowIndex).getState();
                else
                    return "";
            default:
                return callRecordList.get(rowIndex).getId();
        }
    }

}