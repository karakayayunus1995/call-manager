package callrecord;

import main.EventInterface;

import javax.swing.*;
import java.awt.event.ActionListener;

public class CallRecordRightClick extends JPopupMenu {

    public CallRecordRightClick(CallRecord callRecord, EventInterface eventInterface) {

        ActionListener saveAction = (e) -> {
            eventInterface.saveCallRecord(callRecord);
        };

        ActionListener updateAction = (e) -> {
            eventInterface.updateCallRecord(callRecord);
        };

        ActionListener deleteAction = (e) -> {
            String message = "Arama kaydını silmek istediğinize emin misiniz?";
            int reply = JOptionPane.showConfirmDialog( null, message, null, 0);
            if (reply == 0) {
                eventInterface.deleteCallRecord(callRecord);
            }
        };

        JMenuItem update;
        if (callRecord.getCustomerId() == null) {
            update = this.add(new JMenuItem("Kaydet"));
            update.addActionListener(saveAction);
        }
        update = this.add(new JMenuItem("Güncelle"));
        update.addActionListener(updateAction);

        JMenuItem delete = this.add(new JMenuItem("Sil"));
        delete.addActionListener(deleteAction);
    }

}