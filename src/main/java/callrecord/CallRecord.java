package callrecord;

import com.fasterxml.jackson.annotation.JsonFormat;
import customer.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CallRecord")
@Data
@AllArgsConstructor
public class CallRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @Column(name = "telephone", nullable = false)
    @Getter
    @Setter
    private String telephone;

    @Column(name = "note")
    @Getter
    @Setter
    private String note;

    @Column(name = "nick_name")
    @Getter
    @Setter
    private String nickName;

    @Column(name = "state")
    @Getter
    @Setter
    private String state;

    @ManyToOne
    @JoinColumn(name = "customer_ID")
    @Getter
    @Setter
    private Customer customerId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date", nullable = false, columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Setter
    private Date creationDate = new Date();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getCreationDate() {
        return creationDate;
    }

    public CallRecord() {}

    public CallRecord(String telephone) {
        this.telephone = telephone;
    }

    public CallRecord(String telephone, String note, String driverName, String state, Customer customerID) {
        this.telephone = telephone;
        this.note = note;
        this.nickName = driverName;
        this.state = state;
        this.customerId = customerID;
    }
//
    public void setCallRecord(CallRecord callRecord) {
        this.telephone = callRecord.telephone;
        this.note = callRecord.note;
        this.nickName = callRecord.nickName;
        this.state = callRecord.state;
        this.customerId = callRecord.customerId;
    }

}