package callrecord;

import main.CallManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;

public class CallRecordManager {

    public List<CallRecord> addCallRecord(CallRecord callRecord) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<CallRecord> callRecords = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            session.save(callRecord);
            callRecords = listCallRecords();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return callRecords;
    }

    public List<CallRecord> listCallRecords() {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<CallRecord> callRecords = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            callRecords = session.createQuery("FROM CallRecord ORDER BY creationDate DESC").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return callRecords;
    }

    public List<CallRecord> updateCallRecord(CallRecord callRecord) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<CallRecord> callRecords = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            CallRecord newCallRecord = session.get(CallRecord.class, callRecord.getId());
            newCallRecord.setCallRecord(callRecord);
            session.update(newCallRecord);
            callRecords = session.createQuery("FROM CallRecord").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return callRecords;
    }

    public List<CallRecord> deleteCallRecord(Integer callRecordID) {
        Session session = CallManager.factory.openSession();
        Transaction transaction = null;
        List<CallRecord> callRecords = Collections.emptyList();

        try {
            transaction = session.beginTransaction();
            CallRecord callRecord = session.get(CallRecord.class, callRecordID);
            session.delete(callRecord);
            callRecords = session.createQuery("FROM CallRecord").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return callRecords;
    }

    // TODO: 18.08.2021 Ekleme yapacağın zaman localde ekle, sonra database ekle ve database'den çekme 

}