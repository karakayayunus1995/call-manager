package main;

import callrecord.CallRecord;
import customer.Customer;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.util.Collections;

public class CallManager {

    public static SessionFactory factory;

    public static void main(String[] args) {
        new LoginFrame();
    }

    public static void startHibernate(String port) {
        try {
            factory = (new Configuration())
                    .addAnnotatedClass(Customer.class)
                    .addAnnotatedClass(CallRecord.class)
                    .setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect")
                    .setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver")
                    .setProperty("hibernate.connection.url", "jdbc:mysql://localhost:" + port + "/CallManager?createDatabaseIfNotExist=true")
                    .setProperty("hibernate.connection.username", "root")
                    .setProperty("hibernate.connection.password", "root")
                    .setProperty("hibernate.enable_lazy_load_no_trans", "true")
                    .setProperty("hibernate.hbm2ddl.auto", "update")
                    .setProperty("show_sql", "true")
                    .buildSessionFactory();
        } catch (HibernateException ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static void startServerSocket(MainFrame frame) {
        Runnable serverTask = () -> {
            try {
                ServerSocket serverSocket = new ServerSocket(8080);
                frame.setIPAndPortNumber(getIP(), Integer.toString(serverSocket.getLocalPort()));
                while (true) {
                    try {
                        Socket socket = serverSocket.accept();
                        try {
                            DataInputStream dis = new DataInputStream(socket.getInputStream());
                            String str = dis.readUTF();
                            System.out.println("phone number = " + str);
                            frame.callRecordList = frame.CRM.addCallRecord(new CallRecord(str));
                            frame.updateCallRecordTable();
                            if (socket != null)
                                socket.close();
                        } catch (Throwable throwable) {
                            if (socket != null) try {
                                socket.close();
                            } catch (Throwable throwable1) {
                                throwable.addSuppressed(throwable1);
                            }
                            throw throwable;
                        }

                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
                return;
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    private static String getIP() throws SocketException {
        return (Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                .flatMap(i -> Collections.list(i.getInetAddresses()).stream())
                .filter(ip -> (ip instanceof Inet4Address && ip.isSiteLocalAddress()))
                .findFirst().orElseThrow(RuntimeException::new))
                .getHostAddress();
    }

}