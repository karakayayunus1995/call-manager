package main;

import callrecord.CallRecord;
import customer.Customer;

public interface EventInterface {

    void updateCustomer(Customer paramCustomer);

    void deleteCustomer(Customer paramCustomer);

    void saveCallRecord(CallRecord paramCallRecord);

    void updateCallRecord(CallRecord paramCallRecord);

    void deleteCallRecord(CallRecord paramCallRecord);

}