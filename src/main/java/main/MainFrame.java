package main;

import callrecord.CallRecord;
import callrecord.CallRecordManager;
import callrecord.CallRecordRightClick;
import callrecord.CallRecordTableModel;
import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.RenderingMode;
import com.teamdev.jxbrowser.view.swing.BrowserView;
import customer.Customer;
import customer.CustomerManager;
import customer.CustomerRightClick;
import customer.CustomerTableModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;

public class MainFrame implements EventInterface {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private final JTabbedPane tabbedPane = new JTabbedPane();

    private final JTextField edtCustomerName = new JTextField(15);
    private final JTextField edtCustomerAddress = new JTextField(15);
    private final JLabel txtCustomerIPAddress = new JLabel();

    private final JTextField edtCustomerTelephone = new JTextField(15);
    private final JTextField edtCustomerNote = new JTextField(15);
    private final JLabel txtCustomerPortNumber = new JLabel();

    private final JButton btnCustomer = new JButton("KAYDET");
    private final JButton btnSearch = new JButton("ARA");
    private final JButton btnCustomerCancel = new JButton("VAZGEÇ");
    private final JButton btnImport = new JButton("İÇE AKTAR");

    private final JTextField edtCallRecordNickName = new JTextField(15);
    private final JTextField edtCallRecordNote = new JTextField(15);
    private final JLabel txtCallRecordIPAddress = new JLabel();

    private final JTextField edtCallRecordState = new JTextField(15);
    private final JLabel txtCallRecordCustomer = new JLabel();
    private final JLabel txtCallRecordPortNumber = new JLabel();

    private final JButton btnCallRecord = new JButton("GÜNCELLE");
    private final JButton btnCallRecordCancel = new JButton("VAZGEÇ");

    private final JTable tableCustomer = new JTable();
    private final JTable tableCallRecord = new JTable();

    public final CustomerManager CM;
    public final CallRecordManager CRM;

    public List<Customer> customerList;
    public List<CallRecord> callRecordList;

    private CustomerTableModel customerTableModel;
    private CallRecordTableModel callRecordTableModel;

    private Customer selectedCustomer;
    private CallRecord selectedCallRecord;

    private boolean isSaveCustomerOption;           // KAYDET ve GÜNCELLE şeklinde fonksiyonelliği değişen butonun durum bilgisini tutmak için

    public MainFrame() {
        CallManager.startServerSocket(this);
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.add(initPanel());
        frame.pack();
        frame.setVisible(true);

        initListeners();

        CM = new CustomerManager();
        CRM = new CallRecordManager();

        customerList = CM.listCustomers();
        callRecordList = CRM.listCallRecords();

        customerTableModel = new CustomerTableModel(customerList);
        callRecordTableModel = new CallRecordTableModel(callRecordList);

        tableCustomer.setModel(customerTableModel);
        tableCallRecord.setModel(callRecordTableModel);

        isSaveCustomerOption = true;
    }

    private void clearCustomerText() {
        edtCustomerName.setText("");
        edtCustomerTelephone.setText("");
        edtCustomerAddress.setText("");
        edtCustomerNote.setText("");
    }

    private void clearCallRecordText() {
        edtCallRecordNickName.setText("");
        edtCallRecordState.setText("");
        edtCallRecordNote.setText("");
        txtCallRecordCustomer.setText("");
    }

    private void updateCustomerTable() {
        customerTableModel = new CustomerTableModel(customerList);
        tableCustomer.setModel(customerTableModel);
    }

    public void updateCallRecordTable() {
        callRecordTableModel = new CallRecordTableModel(callRecordList);
        tableCallRecord.setModel(callRecordTableModel);
    }

    private void importFile(String fileDirr) {
        try {
            FileInputStream file = new FileInputStream(fileDirr);
            Workbook workbook = new XSSFWorkbook(file);
            Iterator<Row> rowIterator = workbook.getSheetAt(0).iterator();
            DataFormatter dataFormatter = new DataFormatter();
            rowIterator.next();
            while (rowIterator.hasNext()) {
                Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
                Customer customer = new Customer();
                while (cellIterator.hasNext()) {
                    Cell nextCell = cellIterator.next();
                    int columnIndex = nextCell.getColumnIndex();
                    switch (columnIndex) {
                        case 0:                     // name
                            customer.setName(nextCell.getStringCellValue().trim());
                            break;
                        case 1:                     // address
                            customer.setAddress((StringUtils.isNotBlank(nextCell.getStringCellValue()) ? nextCell.getStringCellValue().trim() : null));
                            break;
                        case 2:                     // telephone
                            if (nextCell.getStringCellValue().trim().length() == 11) {
                                customer.setTelephone(nextCell.getStringCellValue().trim());
                            } else if (nextCell.getStringCellValue().trim().length() == 10) {
                                customer.setTelephone("0" + nextCell.getStringCellValue().trim());
                            }
                            break;
                    }
                }
                CM.onlyAddCustomer(customer);
            }
            customerList = CM.listCustomers();
            updateCustomerTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {
        btnCustomer.addActionListener(e -> {
            if (edtCustomerName.getText().trim().equals("") || edtCustomerTelephone.getText().trim().equals("")) {
                JOptionPane.showMessageDialog(btnCustomer, "Müşteri adı ve telefon numarası zorunludur.");
            } else if (edtCustomerTelephone.getText().trim().length() != 11) {
                JOptionPane.showMessageDialog(btnCustomer, "Telefon numarası 11 hane olmak zorundadır.");
            } else {
                Customer customer = new Customer(
                        edtCustomerName.getText().trim(),
                        edtCustomerTelephone.getText().trim(),
                        ((StringUtils.isNotBlank(edtCustomerAddress.getText())) ? edtCustomerAddress.getText() : null),
                        ((StringUtils.isNotBlank(edtCustomerNote.getText())) ? edtCustomerNote.getText() : null)
                );
                if (isSaveCustomerOption) {
                    customerList = CM.addCustomer(customer);
                } else {
                    customer.setId(selectedCustomer.getId());
                    customerList = CM.updateCustomer(customer);
                }
                updateCustomerTable();
                clearCustomerText();
                btnCustomer.setText("KAYDET");
                isSaveCustomerOption = true;
            }
        });

        btnCustomerCancel.addActionListener(e -> {
            clearCustomerText();
            btnCustomer.setText("KAYDET");
            isSaveCustomerOption = true;
            selectedCustomer = null;
        });

        btnImport.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            chooser.setDialogTitle("Veritabanına eklemek istediğiniz excel dosyasını seçiniz");
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Sadece xlsx uzantılı dosyalar", "xlsx");
            chooser.setFileFilter(filter);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (chooser.showOpenDialog(btnImport) == JFileChooser.APPROVE_OPTION) {
                String message = "Dosya içerisindeki kayıtları veri tabanına kaydetmek istediğinize emin misiniz?\nBu işlem geri alınamaz.";
                int reply = JOptionPane.showConfirmDialog(null, message, null, JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    importFile(chooser.getSelectedFile().getPath());
                }
            }
        });

        btnSearch.addActionListener(e -> {
            customerList = CM.searchCustomers(
                    StringUtils.isNotBlank(edtCustomerName.getText()) ? edtCustomerName.getText().trim() : null,
                    StringUtils.isNotBlank(edtCustomerTelephone.getText()) ? edtCustomerTelephone.getText().trim() : null,
                    StringUtils.isNotBlank(edtCustomerAddress.getText()) ? edtCustomerAddress.getText().trim() : null,
                    StringUtils.isNotBlank(edtCustomerNote.getText()) ? edtCustomerNote.getText().trim() : null
            );
            updateCustomerTable();
        });

        tableCustomer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && tableCustomer.getSelectedRow() != -1) {
                    int row = tableCustomer.rowAtPoint(e.getPoint());
                    tableCustomer.setRowSelectionInterval(row, row);
                    selectedCustomer = customerList.get(tableCustomer.getSelectedRow());
                    updateCustomer(selectedCustomer);
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    int row = tableCustomer.rowAtPoint(e.getPoint());
                    if (row >= 0 && row < tableCustomer.getRowCount()) {
                        tableCustomer.setRowSelectionInterval(row, row);
                        selectedCustomer = customerList.get(tableCustomer.getSelectedRow());
                        CustomerRightClick menu = new CustomerRightClick(selectedCustomer, MainFrame.this);
                        menu.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        tableCustomer.clearSelection();
                    }
                }
            }
        });

        btnCallRecord.addActionListener(e -> {
            if (selectedCallRecord != null) {
                CallRecord callRecord = new CallRecord(selectedCallRecord.getTelephone(), edtCallRecordNote.getText(), edtCallRecordNickName.getText(), edtCallRecordState.getText(), null);
                callRecord.setId(selectedCallRecord.getId());
                callRecordList = CRM.updateCallRecord(callRecord);
                updateCallRecordTable();
                clearCallRecordText();
            } else {
                JOptionPane.showMessageDialog(btnCallRecord, "Herhangi bir arama kaydı seçmediniz");
            }
        });

        btnCallRecordCancel.addActionListener(e -> {
            edtCallRecordNickName.setText("");
            edtCallRecordState.setText("");
            edtCallRecordNote.setText("");
            selectedCallRecord = null;
        });

        tableCallRecord.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && tableCallRecord.getSelectedRow() != -1) {
                    int row = tableCallRecord.rowAtPoint(e.getPoint());
                    tableCallRecord.setRowSelectionInterval(row, row);
                    selectedCallRecord = callRecordList.get(tableCallRecord.getSelectedRow());
                    updateCallRecord(selectedCallRecord);
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    int row = tableCallRecord.rowAtPoint(e.getPoint());
                    if (row >= 0 && row < tableCallRecord.getRowCount()) {
                        tableCallRecord.setRowSelectionInterval(row, row);
                        selectedCallRecord = callRecordList.get(tableCallRecord.getSelectedRow());
                        CallRecordRightClick menu = new CallRecordRightClick(selectedCallRecord, MainFrame.this);
                        menu.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        tableCallRecord.clearSelection();
                    }
                }
            }
        });
    }

    @Override
    public void updateCustomer(Customer customer) {
        edtCustomerName.setText(customer.getName());
        edtCustomerTelephone.setText(customer.getTelephone());
        edtCustomerAddress.setText(customer.getAddress());
        edtCustomerNote.setText(customer.getNote());
        btnCustomer.setText("GÜNCELLE");
        isSaveCustomerOption = false;
        selectedCustomer = customer;
    }

    @Override
    public void deleteCustomer(Customer customer) {
        customerList = CM.deleteCustomer(customer.getId());
        updateCustomerTable();
        clearCustomerText();
    }

    @Override
    public void saveCallRecord(CallRecord callRecord) {
        edtCustomerTelephone.setText(callRecord.getTelephone());
        tabbedPane.setSelectedIndex(0);
    }

    @Override
    public void updateCallRecord(CallRecord callRecord) {
        edtCallRecordNickName.setText(callRecord.getNickName());
        edtCallRecordState.setText(callRecord.getState());
        edtCallRecordNote.setText(callRecord.getNote());
        txtCallRecordCustomer.setText(callRecord.getTelephone());
    }

    @Override
    public void deleteCallRecord(CallRecord callRecord) {
        callRecordList = CRM.deleteCallRecord(callRecord.getId());
        updateCallRecordTable();
        clearCallRecordText();
    }

    public void setIPAndPortNumber(String IPAddress, String portNumber) {
        System.out.println("ip: " + IPAddress);
        System.out.println("port: " + portNumber);
        txtCustomerIPAddress.setText(IPAddress);
        txtCustomerPortNumber.setText(portNumber);
        txtCallRecordIPAddress.setText(IPAddress);
        txtCallRecordPortNumber.setText(portNumber);
    }

    private void initMaps() {
        Engine engine = Engine.newInstance(RenderingMode.HARDWARE_ACCELERATED);
        Browser browser = engine.newBrowser();

        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("JxBrowser AWT/Swing");
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    // Shutdown Chromium and release allocated resources.
                    engine.close();
                }
            });
            // Create and embed Swing BrowserView component to display web content.
            frame.add(BrowserView.newInstance(browser));
            frame.setSize(1280, 800);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);

            // Load the required web page.
            browser.navigation().loadUrl("https://html5test.com/");
        });
    }

    private JPanel initPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.insets = new Insets(10, 10, 10, 10);
        tabbedPane.setPreferredSize(new Dimension(screenSize.width - 20, screenSize.height - 150));
        panel.add(tabbedPane, gbc);

        initCustomerPanel();
        initCallRecordPanel();

        return panel;
    }

    private JPanel initCustomerPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(20, 40, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel lblCustomerName = new JLabel("Müşteri adı");
        panel.add(lblCustomerName, gbc);

        gbc.insets = new Insets(20, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(edtCustomerName, gbc);

        gbc.insets = new Insets(20, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        gbc.gridy = 0;
        JLabel lblAddress = new JLabel("Adres");
        panel.add(lblAddress, gbc);

        gbc.insets = new Insets(20, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 4;
        gbc.gridy = 0;
        panel.add(edtCustomerAddress, gbc);

        gbc.insets = new Insets(20, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 6;
        gbc.gridy = 0;
        JLabel lblIPAddress = new JLabel("IP adresi:");
        panel.add(lblIPAddress, gbc);

        gbc.insets = new Insets(20, 0, 15, 40);
        gbc.gridwidth = 1;
        gbc.gridx = 7;
        gbc.gridy = 0;
        panel.add(txtCustomerIPAddress, gbc);

        gbc.insets = new Insets(0, 40, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        JLabel lblTelephone = new JLabel("Telefon");
        panel.add(lblTelephone, gbc);

        gbc.insets = new Insets(0, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(edtCustomerTelephone, gbc);

        gbc.insets = new Insets(0, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        gbc.gridy = 1;
        JLabel lblNote = new JLabel("Not");
        panel.add(lblNote, gbc);

        gbc.insets = new Insets(0, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 4;
        gbc.gridy = 1;
        panel.add(edtCustomerNote, gbc);

        gbc.insets = new Insets(0, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 6;
        gbc.gridy = 1;
        JLabel lblPortNumber = new JLabel("Port numarası:");
        panel.add(lblPortNumber, gbc);

        gbc.insets = new Insets(0, 0, 15, 40);
        gbc.gridwidth = 1;
        gbc.gridx = 7;
        gbc.gridy = 1;
        panel.add(txtCustomerPortNumber, gbc);

        gbc.insets = new Insets(0, 20, 15, 20);
        gbc.gridwidth = 5;
        gbc.gridx = 0;
        gbc.gridy = 2;

        JPanel panelBtn = new JPanel();
        panelBtn.setLayout(new GridBagLayout());
        GridBagConstraints gbcBtn = new GridBagConstraints();
        gbcBtn.fill = GridBagConstraints.HORIZONTAL;

        gbcBtn.insets = new Insets(0, 0, 0, 20);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 0;
        gbcBtn.gridy = 0;
        panelBtn.add(btnCustomer, gbcBtn);

        gbcBtn.insets = new Insets(0, 0, 0, 20);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 1;
        gbcBtn.gridy = 0;
        panelBtn.add(btnSearch, gbcBtn);

        gbcBtn.insets = new Insets(0, 0, 0, 20);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 2;
        gbcBtn.gridy = 0;
        panelBtn.add(btnCustomerCancel, gbcBtn);

        gbcBtn.insets = new Insets(0, 0, 0, 0);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 3;
        gbcBtn.gridy = 0;
        panelBtn.add(btnImport, gbcBtn);

        panel.add(panelBtn, gbc);

        gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridwidth = 10;
        gbc.gridheight = 10;
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        JScrollPane scrollPane = new JScrollPane(tableCustomer);
        panel.add(scrollPane, gbc);

        tableCustomer.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
        tableCustomer.setRowHeight(25);

        tabbedPane.addTab("Müşteriler", panel);

        return panel;
    }

    private JPanel initCallRecordPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(20, 40, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel lblNickName = new JLabel("Takma adı");
        panel.add(lblNickName, gbc);

        gbc.insets = new Insets(20, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(edtCallRecordNickName, gbc);

        gbc.insets = new Insets(20, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        gbc.gridy = 0;
        JLabel lblNote = new JLabel("Not");
        panel.add(lblNote, gbc);

        gbc.insets = new Insets(20, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 4;
        gbc.gridy = 0;
        panel.add(edtCallRecordNote, gbc);

        gbc.insets = new Insets(20, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 6;
        gbc.gridy = 0;
        JLabel lblIPAddress = new JLabel("IP adresi:");
        panel.add(lblIPAddress, gbc);

        gbc.insets = new Insets(20, 0, 15, 40);
        gbc.gridwidth = 1;
        gbc.gridx = 7;
        gbc.gridy = 0;
        panel.add(txtCallRecordIPAddress, gbc);

        gbc.insets = new Insets(0, 40, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        JLabel lblState = new JLabel("Durum bilgisi");
        panel.add(lblState, gbc);

        gbc.insets = new Insets(0, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(edtCallRecordState, gbc);

        gbc.insets = new Insets(0, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        gbc.gridy = 1;
        JLabel lblCustomer = new JLabel("Müşteri");
        panel.add(lblCustomer, gbc);

        gbc.insets = new Insets(0, 0, 15, 20);
        gbc.gridwidth = 2;
        gbc.gridx = 4;
        gbc.gridy = 1;
        panel.add(txtCallRecordCustomer, gbc);

        gbc.insets = new Insets(0, 0, 15, 10);
        gbc.gridwidth = 1;
        gbc.gridx = 6;
        gbc.gridy = 1;
        JLabel lblPortNumber = new JLabel("Port numarası:");
        panel.add(lblPortNumber, gbc);

        gbc.insets = new Insets(0, 0, 15, 40);
        gbc.gridwidth = 1;
        gbc.gridx = 7;
        gbc.gridy = 1;
        panel.add(txtCallRecordPortNumber, gbc);

        gbc.insets = new Insets(0, 20, 15, 20);
        gbc.gridwidth = 5;
        gbc.gridx = 0;
        gbc.gridy = 2;

        JPanel panelBtn = new JPanel();
        panelBtn.setLayout(new GridBagLayout());
        GridBagConstraints gbcBtn = new GridBagConstraints();
        gbcBtn.fill = GridBagConstraints.HORIZONTAL;

        gbcBtn.insets = new Insets(0, 0, 0, 20);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 0;
        gbcBtn.gridy = 0;
        panelBtn.add(btnCallRecord, gbcBtn);

        gbcBtn.insets = new Insets(0, 0, 0, 20);
        gbcBtn.gridwidth = 1;
        gbcBtn.gridx = 1;
        gbcBtn.gridy = 0;
        panelBtn.add(btnCallRecordCancel, gbcBtn);

        panel.add(panelBtn, gbc);

        gbc.insets = new Insets(20, 20, 20, 20);
        gbc.gridwidth = 9;
        gbc.gridheight = 9;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        JScrollPane scrollPane = new JScrollPane(tableCallRecord);
        panel.add(scrollPane, gbc);

        tableCallRecord.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        tableCallRecord.setRowHeight(25);

        tabbedPane.addTab("Arama Kayıtları", panel);

        return panel;
    }

}