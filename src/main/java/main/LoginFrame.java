package main;

import models.LoginRequest;
import models.LoginResponse;
import org.apache.commons.lang3.StringUtils;
import retrofit.APIClient;
import retrofit.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class LoginFrame {

    private final JFrame frame = new JFrame();

    private final JTextField edtMySqlPort = new JTextField(15);

    private final JTextField edtPhone = new JTextField(15);

    private final JPasswordField edtPassword = new JPasswordField(15);

    private final JCheckBox checkBox = new JCheckBox("Şifreyi göster");

    private final JButton btnLogin = new JButton("GİRİŞ YAP");

    public LoginFrame() {
        frame.add(initPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        initListeners();
    }

    private void initListeners() {
        checkBox.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                edtPassword.setEchoChar((char) 0);
            } else {
                edtPassword.setEchoChar('*');
            }
        });

        btnLogin.addActionListener(e -> {
            if (StringUtils.isNotBlank(edtMySqlPort.getText()) && StringUtils.isNotBlank(edtPhone.getText()) && StringUtils.isNotBlank(edtPassword.getText())) {
                sendRequest(edtPhone.getText(), edtPassword.getText());
            } else {
                JOptionPane.showMessageDialog(btnLogin, "Gerekli numara, kullanıcı adı ve şifre zorunludur.");
            }
        });
    }

    private void sendRequest(String phone, String password) {
        UserService userService = APIClient.getClient().create(UserService.class);

        Call<LoginResponse> call = userService.login(new LoginRequest(phone, password));
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200) {
                    CallManager.startHibernate(edtMySqlPort.getText().trim());
                    new MainFrame();
                    frame.dispose();
                } else {
                    JOptionPane.showMessageDialog(btnLogin, "Girdiğiniz bilgiler geçersizdir. Kontrol ediniz.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                System.out.println("BAŞARISIZ");
            }
        });
    }

    private void openBrowser() {
        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(new URI("https://www.google.com/maps"));
            }
        } catch (URISyntaxException | IOException exception) {

        }
    }

    private JPanel initPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(40, 40, 15, 30);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel lblMySqlPort = new JLabel("Gerekli numara");
        panel.add(lblMySqlPort, gbc);

        gbc.insets = new Insets(40, 0, 15, 40);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(edtMySqlPort, gbc);

        gbc.insets = new Insets(0, 40, 15, 30);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        JLabel lblPhone = new JLabel("Telefon");
        panel.add(lblPhone, gbc);

        gbc.insets = new Insets(0, 0, 15, 40);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(edtPhone, gbc);

        gbc.insets = new Insets(0, 40, 15, 30);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 2;
        JLabel lblPassword = new JLabel("Şifre");
        panel.add(lblPassword, gbc);

        gbc.insets = new Insets(0, 0, 15, 40);
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 2;
        panel.add(edtPassword, gbc);

        gbc.insets = new Insets(0, 0, 30, 0);
        gbc.gridwidth = 3;
        gbc.gridy = 3;
        panel.add(checkBox, gbc);

        gbc.insets = new Insets(0, 0, 40, 0);
        gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 4;
        panel.add(btnLogin, gbc);

        return panel;
    }

}