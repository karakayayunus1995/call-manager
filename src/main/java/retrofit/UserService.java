package retrofit;

import models.LoginRequest;
import models.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserService {

    @Headers({"Accept: application/json"})
    @POST("auth/login")
    Call<LoginResponse> login(
            @Body LoginRequest paramLoginRequest
    );

}